Xrft
----

Before jumping into the notebook, it is recommended to go through the `Holoviews tutorial
<exploration_with_holoviews.html>`_ first.

.. toctree::
    ../notebooks/xrft.ipynb
