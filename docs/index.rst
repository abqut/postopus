========
postopus
========

This is the documentation of `Postopus <https://gitlab.com/octopus-code/postopus/>`_ (the POST-processing of OctoPUS data) is an environment that
can help with the analysis of data that was computed using the `Octopus <https://octopus-code.org>`_ package.


Contents
========

.. toctree::
   :maxdepth: 2

   Quick Start <Quick_Start/index>
   User Guide <User_Guide/index>
   License <license>
   Authors <authors>
   Change log <changelog>
   FAQ <faq>
   Module Reference <api/modules>





Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`


About this documentation
========================

.. note::

    This is still work in progress.
