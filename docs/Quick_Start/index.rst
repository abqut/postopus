Quick_Start
-----------

The following notebook just quickly summarizes the most important functionalities that
postopus provides. If you want to dive deeper we recommend visiting the `User Guide
<../User_Guide/index.html>`_

.. toctree::
    Quickstart with postopus <Quick_Start>
