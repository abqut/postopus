import pathlib
import sys
import warnings
from pathlib import Path
from typing import Any, Dict, List, Tuple, Union

import numpy as np
import pandas
import xarray

from postopus.datacontainers.util.convenience_dict import ConvenienceDict
from postopus.files import openfile
from postopus.namespace_utils import build_namespaces
from postopus.utils import check_avail_memory, humanise_size, parser_log_retrieve_value


class ScalarField:
    def __init__(
        self,
        ftype: str,
        path: pathlib.Path,
        systemname: str,
        calculationmode: str,
        fields_in_mode: Dict[
            str, Union[Tuple[str, List[str]], List[pathlib.Path], List[str]]
        ],
    ) -> None:
        """
        Class represents a scalar field. Will find all the iterations where this field
        is written out and provides methods to load data.

        Parameters
        ----------
        ftype : str
            type of field stored in the object. Name of the field equals the
            name of the file written by Octopus (without file suffix and
            dimension of the vector "-[xyz]").
        path : pathlib.Path
            /path/to/octopus/output (containing inp)
        systemname : str
            Name of the system containing this field
        calculationmode : str
            CalculationMode value of the run that wrote this field
        fields_in_mode : Dict[
                            str, Union[
                                Tuple[str, List[str]], List[pathlib.Path], List[str]
                            ]
                         ]
            All information on this field, as found in OutputCollector.

        """
        self.ftype = ftype
        self._path = path
        self._calculationmode = calculationmode
        self._systemname = systemname

        self._filename = self.ftype

        # count iterations, where we can find an output file containing this
        # field (method from super)
        iterations_store = [
            iter
            for iter, fields in fields_in_mode["iterations"]
            if self.ftype in fields
        ]
        # iterations_store could be empty, when only 'static' or 'td.general' exists,
        # but no actual content in output_iter
        if iterations_store != [""]:
            self.iteration_ids = tuple(sorted([int(i) for i in iterations_store]))
        else:
            self.iteration_ids = tuple()
        self.n_iterations = len(self.iteration_ids)
        # try because there might be no output_iter, only "static" or "td.general"
        try:
            self.digits_in_iter_id = len(iterations_store[0])
        except IndexError:
            self.digits_in_iter_id = 0

        # CalculationMode 'gs' in Octopus writes the final results to the 'static'
        # folder on the same level as 'inp'
        # TODO: This will change in the future, 'static' will be in system folders
        if calculationmode == "scf":
            try:
                self._static_avail = not fields_in_mode["static"] == []
            except KeyError:
                # happens if no folder "static" exists
                self._static_avail = False
            if self._static_avail:
                # if we have static data we need to add it as a dedicated iteration
                # in self.iterations, otherwise loading with get is impossible.
                step_size = np.diff(self.iteration_ids)
                if len(set(step_size)) == 1:
                    # all steps are equidistant, we can simply add a new step
                    # parse it from a np.int64 to python int
                    static_iter_num = self.iteration_ids[-1] + int(step_size[0])
                else:
                    if (
                        parser_log_retrieve_value(
                            self._path / "exec" / "parser.log",
                            "OutputDuringSCF",
                            conversion=int,
                        )
                        == 1
                    ):
                        # not all steps are spaced apart the same distance. Inform
                        # the user
                        warnings.warn(
                            "Your scf data might have missing simulation steps. "
                            "Distance between all steps is not consistent. Still "
                            "loading your data. The converged field will have 'step = "
                            "number_of_last_found_step + 1'.",
                            UserWarning,
                        )
                    try:
                        static_iter_num = self.iteration_ids[-1] + 1
                    except IndexError:
                        # this happens when no iterations are in self.iteration_ids,
                        # e. g. with benzene example.
                        # Just add iteration '1', good enough.
                        static_iter_num = 1
                # set new values for self.iteration_ids and self.n_iterations
                tmp_iters = list(self.iteration_ids)
                tmp_iters.append(static_iter_num)
                self.iteration_ids = tuple(tmp_iters)
                self.n_iterations = len(self.iteration_ids)
        else:
            # check if all outputs are there or if we are missing a step in the data
            step_size = np.diff(self.iteration_ids)
            if len(set(step_size)) != 1:
                warnings.warn(
                    "Your data might have missing simulation steps. Distance "
                    "between all steps is not consistent. Still loading "
                    "your data."
                )
            self._static_avail = None

    def _get_single_step(
        self, step: int = None, source: str = "auto"
    ) -> xarray.DataArray:
        """
        Load field data from the specified iteration.

        The time coordinate values are converted to a natural/physical time by
        multiplying the step number with the magnitude of one timestep.

        Parameters
        ----------
        step : int
            index of iteration (Default value = None)
        source : str
            file suffix of the file to load (Default value = "auto")

        Returns
        -------
        xarray.DataArray
            Field for the chosen iteration in a Xarray.DataArray

        """
        # step not provided and static data does not exist
        if step is None and self._static_avail is False:
            # CalculationMode is 'gs', but there is no static data available
            # (maybe deleted or moved?)
            raise ValueError(
                "There is no static data available, please provide "
                "a value for iter_index."
            )

        # calculationmode is not 'scf' (results in self._static_avail = None)
        # and step was not provided
        if step is None and self._static_avail is None:
            # raised, if step is not filled and CalculationMode is not 'gs'
            raise ValueError(
                "Please provide a value for iter_index! Only not required "
                "if CalculationMode is 'gs'."
            )

        # (step not provided and static data exists) or (static iteration number was
        #                                                provided)
        if (step is None and self._static_avail is True) or (
            step == self.iteration_ids[-1] and self._static_avail is True
        ):
            step = self.iteration_ids[-1]  # in case it was None
            xa = self._load_file(step, source).xarray()

            possible_namespaces = build_namespaces(self._systemname)
            calc_mode_step, name_step_coord = self._get_xarray_steps_label(
                possible_namespaces, [self.iteration_ids[-1]]
            )

            xa = xarray.concat(
                [xa],
                pandas.Index(calc_mode_step, name=name_step_coord),
            )

            return xa

        # else: step provided but not the converged one
        possible_namespaces = build_namespaces(self._systemname)
        calc_mode_step, name_step_coord = self._get_xarray_steps_label(
            possible_namespaces, [step]
        )

        return xarray.concat(
            [self._load_file(step, source).xarray()],
            pandas.Index(calc_mode_step, name=name_step_coord),
        )

    def _get_steps_from_list(
        self, load_steps: List[int], source: str = "auto"
    ) -> xarray.DataArray:
        """
        Load field data for a list of simulation steps.

        The time coordinate values are converted to a natural/physical time by
        multiplying the step number with the magnitude of one timestep.

        Parameters
        ----------
        load_steps : List[int]
            List of simulation steps to load for this field
        source : str
            file suffix of the file to load (Default value = "auto")

        Returns
        -------
        xarray.DataArray
            Field for all chosen iteration in an Xarray.DataArray

        """
        file0 = self._load_file(load_steps[0], source)
        arrs = np.empty(
            shape=(len(load_steps), *file0.values.shape), dtype=file0.values.dtype
        )
        arrs[0] = file0.values

        for indx, iter in enumerate(load_steps[1:]):
            fileobj = self._load_file(iter, source)
            arrs[indx + 1] = fileobj.values  # +1, because file0 already loaded

        possible_namespaces = build_namespaces(self._systemname)
        calc_mode_steps, name_step_coord = self._get_xarray_steps_label(
            possible_namespaces, load_steps
        )
        coords = {name_step_coord: np.array(calc_mode_steps), **file0.coords}
        dims = (name_step_coord, *file0.dims)

        return xarray.DataArray(
            arrs, coords=coords, dims=dims, name=Path(file0.filepath).stem
        )

    def _load_file(self, step: int, source: str) -> Any:
        """
        Build the path for a file and loads it using Postopus' openfile.

        Parameters
        ----------
        step : int
            simulation step number
        source : str
            filetype

        Returns
        -------
        Any
            file object from module `postopus.files`

        """
        if (
            step == self.iteration_ids[-1]
            and self._calculationmode == "scf"
            and self._static_avail
        ):
            # this iter is the converged iteration. It is not stored in
            # 'output_iter', but in 'static'!
            # TODO: This path will change, when Octopus moves output of static into
            #  the systems themselves
            path_without_source = pathlib.Path(
                self._path,
                "static",
                self._filename,
            )
        else:
            # Build the path to the file containing the field from predetermined
            # values. Compatible with single and multisystems.
            sysname_in_path = ""
            if self._systemname != "default":
                sysname_in_path = self._systemname.split(".")

            path_without_source = pathlib.Path(
                self._path,
                *sysname_in_path,
                "output_iter",
                self._calculationmode + "." + str(step).zfill(self.digits_in_iter_id),
                self._filename,
            )

        if source == "auto":
            source = self._get_auto_source(path_without_source)
        source = "." + source
        filepath = path_without_source.with_suffix(source)
        return openfile(filepath)

    def _get_auto_source(self, path_without_source):
        """
        Gets automatic source.

        Checks how many sources/extensions of the same field live in the parent
        directory. If there is only one available source,this is selected as the
        automatic source. Otherwise prints out the available sources for the user.


        Parameters
        ----------
        path_without_source path to the field without the source.

        Returns
        -------
        Either a unique source or a ValueError.

        """
        avail_sources = [
            path.suffix[1:]  # without dot
            for path in path_without_source.parent.iterdir()
            if path_without_source.stem in str(path)
        ]
        if len(avail_sources) == 1:
            return avail_sources[0]
        else:
            raise ValueError(
                "There is more than one available source for the selected file.  \n"
                "You need to add the parameter source=... to the function call.  \n"
                f"Following available sources were found: {avail_sources}"
            )

    def _get(
        self,
        steps: Union[str, int, List[int]] = None,
        source: str = "auto",
        ignore_warnings: bool = False,
    ) -> xarray.DataArray:
        """
        Private Method used to load data for a field. Fields can be loaded for one
        or multiple simulation steps.

        Parameters
        ----------
        steps : Union[str, int, List[int]]
            Which simulation steps to load. Either a single step, a list of steps or
            string "all" to select all available. Providing no value for `steps` works
            with the "scf" calculation mode and will return converged fields.
        source : str
            Target output format for the field. E. g. "z=0", "cube", "vtk", ...
        ignore_warnings : bool
            Choose whether to ignore warnings that to much memory might be used when
            loading.

        Returns
        -------
        xarray.DataArray
            Xarray contains the field, and an additional coordinate "step" for selecting
            the simulation steps.

        """

        if isinstance(steps, int):
            # load a single step
            # check if requested step exists
            self._check_steps_exist([steps])
            # check if enough memory is available
            if not ignore_warnings:
                self._check_required_memory([steps], source)
            return self._get_single_step(steps, source)
        elif isinstance(steps, list):  # we already checked is a list of ints in get()
            # load all steps from provided list
            # check if requested step exists
            self._check_steps_exist(steps)
            # check if enough memory is available
            if not ignore_warnings:
                self._check_required_memory(steps, source)
            return self._get_steps_from_list(steps, source)
        elif steps is None:
            # load data from scf
            if not ignore_warnings:
                self._check_required_memory([self.iteration_ids[-1]], source)
            return self._get_single_step(source=source)
        elif steps == "all":  # only string "all" is accepted
            # load all available simulation steps
            # check if enough memory is available
            if not ignore_warnings:
                self._check_required_memory(self.iteration_ids, source)
            return self._get_steps_from_list(self.iteration_ids, source)

    def get_all(
        self,
        source: str = "auto",
        ignore_warnings: bool = False,
    ) -> xarray.DataArray:
        """
        Load all available simulation steps (including the converged step)

        Also check if enough memory is available.

        Parameters
        ----------
        source : str
            Target output format for the field. E. g. "z=0", "cube", "vtk", ...
        ignore_warnings : bool
            Choose whether to ignore warnings that to much memory might be used when
            loading.
        Returns
        -------
        xarray.DataArray
            Xarray contains the field, and an additional coordinate "step" or "t" (if
            time dependent) for selecting the simulation steps.

        Examples
        --------
        >>> from pathlib import Path; from postopus import Run
        >>> repodir = Path("path_to_repodir")
        >>> testdata_dir = repodir / "tests" / "data" / "methane"
        >>> run = Run(testdata_dir / "benzene")
        >>> run.default.scf.density.get_all(source="xsf")
        >>> <xarray.DataArray...

        """
        return self._get("all", source, ignore_warnings)

    def get_converged(
        self,
        source: str = "auto",
        ignore_warnings: bool = False,
    ) -> xarray.DataArray:
        """
        Get converged scf iteration

        Parameters
        ----------
        source : str
            Target output format for the field. E. g. "z=0", "cube", "vtk", ...

        Returns
        -------
        xarray.DataArray
            Xarray contains the field, and an additional coordinate "step" or "t" (if
            time dependent) for selecting
            the simulation steps.

        Examples
        --------
        >>> from pathlib import Path; from postopus import Run
        >>> repodir = Path("path_to_repodir")
        >>> testdata_dir = repodir / "tests" / "data" / "methane"
        >>> run = Run(testdata_dir)
        >>> run.default.scf.density.get_converged(source="xsf")
        >>> <xarray.DataArray...

        """
        return self._get(steps=None, source=source, ignore_warnings=ignore_warnings)

    def get(
        self,
        steps: Union[int, List[int]] = None,
        source: str = "auto",
        ignore_warnings: bool = False,
    ) -> xarray.DataArray:
        """
        Method used to load data for a field. Fields can be loaded for one or multiple
        simulation steps.

        Parameters
        ----------
        steps : Union[int, List[int]]
            Which simulation steps to load. Either a single step, a list of steps.
        source : str
            Target output format for the field. E. g. "z=0", "cube", "vtk", ...
        ignore_warnings : bool
            Choose whether to ignore warnings that to much memory might be used when
            loading.

        Returns
        -------
        xarray.DataArray
            Xarray contains the field, and an additional coordinate "step"or "t" (if
            time dependent) for selecting
            the simulation steps.

        Examples
        --------
        Single iteration:

        >>> from pathlib import Path; from postopus import Run
        >>> repodir = Path("path_to_repodir")
        >>> testdata_dir = repodir / "tests" / "data" / "methane"
        >>> run = Run(testdata_dir)
        >>> run.default.scf.density.get(steps=1, source="xsf")
        >>> <xarray.DataArray...

        List of iterations:

        >>> run.default.scf.density.get(steps=[1, 2], source="xsf")
        >>> <xarray.DataArray...

        """
        if isinstance(steps, int) or (
            isinstance(steps, list) and all(isinstance(step, int) for step in steps)
        ):
            return self._get(steps, source, ignore_warnings)
        else:
            raise TypeError(
                "steps parameter needs to be provided. It needs to be either an integer"
                " or a list of integers. \n   Hint: You can also make use of"
                " get_all(),"
                " get_converged(),"
                " or iget()."
            )

    def iget(
        self,
        indices: Union[int, List[int], slice] = None,
        source: str = "auto",
        ignore_warnings: bool = False,
    ):
        """
        Method used to load data for a field. Fields can be loaded for one or multiple
        simulation steps. The selection is done by index.

        Parameters
        ----------
        indices: Union[int, List[int], slice]
            Which simulation steps to load, by index. Either a single index,
            a list of indices or a slice(int, int) object. Negative integers are also
            allowed.
        source : str
            Target output format for the field. E. g. "z=0", "cube", "vtk", ...
        ignore_warnings : bool
            Choose whether to ignore warnings that to much memory might be used when
            loading.

        Returns
        -------
        xarray.DataArray
            Xarray contains the field, and an additional coordinate "step"or "t" (if
            time dependent) for selecting
            the simulation steps.

        Examples
        --------
        Single iteration:

        >>> from pathlib import Path; from postopus import Run
        >>> repodir = Path("path_to_repodir")
        >>> testdata_dir = repodir / "tests" / "data" / "methane"
        >>> run = Run(testdata_dir)
        >>> run.default.scf.density.iget(-1, source="xsf")
        >>> <xarray.DataArray...

        List of iterations:

        >>> run.default.scf.density.iget([5, 7], "ncdf")
        >>> <xarray.DataArray...  # two iterations

        Slice:

        >>> run.default.scf.density.iget(slice(0, 3), "ncdf")
        >>> <xarray.DataArray...  # three iterations


        """
        if isinstance(indices, int):
            return self._get(self.iteration_ids[indices], source, ignore_warnings)
        elif isinstance(indices, slice):
            return self._get(list(self.iteration_ids[indices]), source, ignore_warnings)
        elif isinstance(indices, list) and all(
            isinstance(index, int) for index in indices
        ):
            selected_indices = [self.iteration_ids[index] for index in indices]
            return self._get(selected_indices, source, ignore_warnings)
        else:
            raise TypeError(
                "indices parameter needs to be provided. It needs to be either an"
                " integer (negative also allowed),"
                " a list of integers, or a slice(int, int) object. \n"
                " Hint: You can also make use of"
                " get_all(),"
                " get_converged(),"
                " or get()."
            )

    def _check_required_memory(self, load_steps: List[int], source: str):
        """
        Check if all desired fields can be loaded. Calculating required size of memory
        by checking size of the field for a single iteration and multiplying by the
        number of steps to load.
        If required memory exceeds available memory, proactively raise a MemoryError.
        This MemoryError can be ignored by providing `ignore_warnings` to get()

        Parameters
        ----------
        load_steps : List[int]
            List of all simulation steps that shall be loaded
        source : str
            File format for loading

        Returns
        -------
        None
            Raises a MemoryError if things go south and the user wants to allocate to
            much memory - but will not return anything per se.
        """
        # Check if data fits into available memory
        single_file = self._load_file(load_steps[0], source)
        # size of values + size of list of dims + size of coords np.arrays
        # this is not 100% on point (would need recursive summation, e. g. every string
        # in single_file.dims + single_file.dims size, which is a list), but it gets the
        # larger chunks
        size_in_mem = (
            single_file.values.nbytes
            + sys.getsizeof(single_file.dims)
            + sum([arr.nbytes for arr in single_file.coords.values()])
        )

        if not check_avail_memory(size_in_mem * len(load_steps)):
            tot_size = humanise_size(size_in_mem * len(load_steps))
            raise MemoryError(
                f"You are trying to load (approx.) {tot_size[0]} {tot_size[1]}!"
                f"Your system has less memory available than this. You "
                f"have been warned - but if you still want to try, set the "
                f"'ignore_warnings' parameter to True (worst case: your program will"
                f"crash. Best case: might work :) )."
            )

    def _check_steps_exist(self, load_steps: List[int]):
        """
        Checks if the provided list of steps exists in the output of the Octopus output

        Parameters
        ----------
        load_steps : List[int]
            List of steps which shall be loaded

        """
        for step in load_steps:
            # Check if data exists for the requested iteration
            if step not in self.iteration_ids:
                raise ValueError(
                    f"Requested iteration '{step}' does not exist for '{self.ftype}'."
                )

    def _get_xarray_steps_label(self, namespaces: List[str], load_steps: List[int]):
        """
        Get the step label (coord values + dim name) for the xarray

        The iteration/step dimension depends on the calculation mode that we are
        dealing with. For example, in the calculation mode "td" we have an actual time
        dimension, whereas in the "scf" mode we are just dealing with optimization
        steps and we don't have any physical time, so we would return the load_step
        unchanged.

        For the td mode we retrieve the TDTimeStep value from the parser log file.
        The searching routine tries all the possible namespaces for finding it.
        Parameters
        ----------
        namespaces: All possible namespaces given the present systemname. Only used for
        td calculation mode.
        load_steps: Loading steps as in filesystem structure

        Returns
        -------
        - steps (coordinate values) intrinsic to the calculation mode that we are
         dealing with
        - the name of the intrinsic step variable (dimension name)

        """
        path_to_parser_log_file = pathlib.Path(f"{self._path}/exec/parser.log")
        if self._calculationmode == "td":
            try:
                sysname = namespaces[-1]
                namespaces.pop(-1)
                timestep_key = f"{sysname}.TDTimeStep"
                timestep = parser_log_retrieve_value(
                    path_to_parser_log_file, timestep_key, conversion=float
                )
            except ValueError:
                if namespaces:
                    # TODO: Not yet tested, we don't have examples
                    self._get_xarray_steps_label(namespaces, load_steps)
                else:
                    timestep = parser_log_retrieve_value(
                        path_to_parser_log_file, "TDTimeStep", conversion=float
                    )
            load_time_steps = [load_step * timestep for load_step in load_steps]
            return load_time_steps, "t"
        else:
            # TODO: Not yet tested, we don't have examples
            if self._calculationmode != "scf":
                warnings.warn(
                    f"We don't have any practical experience with "
                    f"{self._calculationmode} calculation modes. We are assuming that "
                    f"no "
                    f"physical units are associated with "
                    f"the iteration/step dimension. "
                    f"So we will just call it 'step' and it will just consist"
                    f" of a list of integers, imitating"
                    f" the filesystem's structure."
                )
            return load_steps, "step"


class VectorField(ConvenienceDict):
    __dict_name__ = "components"

    def __init__(
        self,
        ftype: str,
        path: pathlib.Path,
        systemname: str,
        calculationmode: str,
        fields_in_mode: Dict[
            str, Union[Tuple[str, List[str]], List[pathlib.Path], List[str]]
        ],
    ) -> None:
        """
        Vectorfields in Octopus are written out in three separate files. Actual fields
        (field data and values) are contained in VectorFieldDim for every direction
        (x, y, z), this class provides access to these directions.

        Parameters
        ----------
        ftype : str
            type of field stored in the object (e. g. 'magnetic_field',
            'electric_field', ...). Naming from Octopus' input file 'inp'.
        path : pathlib.Path
            /path/to/output_iter  or  /path/to/System_name_in_inp
        systemname : str
            Name of the system containing this field
        calculationmode : str
            CalculationMode value of the run that wrote this field
        fields_in_mode : Dict[
                            str, Union[
                                Tuple[str, List[str]], List[pathlib.Path], List[str]
                            ]
                         ]
            All information on this field, as found in OutputCollector

        """
        super().__init__()

        self.ftype = ftype
        self._path = path
        self._calculationmode = calculationmode
        self._systemname = systemname

        # Build the components out of ScalarFields, because they basically are just that
        self.components = {
            dim: ScalarField(
                ftype + "-" + dim, path, systemname, calculationmode, fields_in_mode
            )
            for dim in "xyz"
        }

        x_iterations = self.components["x"].iteration_ids
        y_iterations = self.components["y"].iteration_ids
        z_iterations = self.components["z"].iteration_ids

        if not x_iterations == y_iterations or not y_iterations == z_iterations:
            msg = (
                f"Error: inconsistent number of files found for file_type {ftype}.\n"
                f"Please check for missing simulation steps!\n"
                f"Found components and steps:\n"
            )
            msg += self._output_iter_difference(
                x_iterations, y_iterations, z_iterations
            )
            raise FileNotFoundError(msg)

    def get_all(
        self,
        source: str = "auto",
        ignore_warnings: bool = False,
    ) -> xarray.DataArray:
        """
        Load all available simulation steps (including the converged step)

        Also check if enough memory is available.

        Parameters
        ----------
        source : str
            Target output format for the field. E. g. "z=0", "cube", "vtk", ...
        ignore_warnings : bool
            Choose whether to ignore warnings that to much memory might be used when
            loading.
        Returns
        -------
        xarray.DataArray
            Xarray contains the field, and an additional coordinate "step" or "t" (if
            time dependent) for selecting the simulation steps.
        Examples
        --------
        >>> from pathlib import Path; from postopus import Run
        >>> repodir = Path("path_to_repodir")
        >>> testdata_dir = repodir / "tests" / "data" / "methane"
        >>> run = Run(testdata_dir)
        >>> run.default.td.current.get_all(source="ncdf")
        >>> <xarray.DataArray...  # contains all 3 components + coordinate "t"

        """
        return self._get("all", source, ignore_warnings)

    def get(
        self,
        steps: Union[int, List[int]] = None,
        source: str = "auto",
        ignore_warnings: bool = False,
    ) -> xarray.DataArray:
        """
        Method used to load data for a field. Fields can be loaded for one or multiple
        simulation steps. As this method is related to VectorField it will load all
        components of the vector field

        Parameters
        ----------
        steps : Union[int, List[int]]
            Which simulation steps to load. Either a single step, a list of steps.
        source : str
            Target output format for the field. E. g. "z=0", "cube", "vtk", ...
        ignore_warnings : bool
            Choose whether to ignore warnings that too much memory might be used when
            loading.

        Returns
        -------
        xarray.DataArray
            Xarray contains the field, and an additional coordinate "step" or "t" (if
            time dependent) for selecting
            the simulation steps.
        Examples
        --------
        Single iteration:

        >>> from pathlib import Path; from postopus import Run
        >>> repodir = Path("path_to_repodir")
        >>> testdata_dir = repodir / "tests" / "data" / "methane"
        >>> run = Run(testdata_dir)
        >>> run.default.td.current.get(steps=1, source="ncdf")
        >>> <xarray.DataArray...

        List of iterations:

        >>> run.default.td.current.get(steps=[1, 2], source="ncdf")
        >>> <xarray.DataArray...  # contains all 3 components + coordinate "t"

        """
        if isinstance(steps, int) or (
            isinstance(steps, list) and all(isinstance(step, int) for step in steps)
        ):
            return self._get(steps, source, ignore_warnings)
        else:
            raise TypeError(
                "steps parameter needs to be provided. It needs to be either an integer"
                " or a list of integers. \n   Hint: You can also make use of"
                " get_all(),"
                " or iget()."
            )

    def iget(
        self,
        indices: Union[int, List[int], slice] = None,
        source: str = "auto",
        ignore_warnings: bool = False,
    ):
        """
        Method used to load data for a field. Fields can be loaded for one or multiple
        simulation steps. The selection is done by index.

        Parameters
        ----------
        indices: Union[int, List[int], slice]
            Which simulation steps to load, by index. Either a single index,
            a list of indices or a slice(int, int) object. Negative integers are also
            allowed.
        source : str
            Target output format for the field. E. g. "z=0", "cube", "vtk", ...
        ignore_warnings : bool
            Choose whether to ignore warnings that to much memory might be used when
            loading.

        Returns
        -------
        xarray.DataArray
            Xarray contains the field, and an additional coordinate "step"or "t" (if
            time dependent) for selecting
            the simulation steps.

        Examples
        --------
        Single iteration:

        >>> from pathlib import Path; from postopus import Run
        >>> repodir = Path("path_to_repodir")
        >>> testdata_dir = repodir / "tests" / "data" / "methane"
        >>> run = Run(testdata_dir)
        >>> run.default.td.current.iget(-1, "ncdf")
        >>> <xarray.DataArray... # one iteration

        List of iterations:

        >>> run.default.td.current.iget([5, 7], "ncdf")
        >>> <xarray.DataArray...  # two iterations

        Slice:

        >>> run.default.td.current.iget(slice(0, 3), "ncdf")
        >>> <xarray.DataArray...  # three iterations






        """
        if isinstance(indices, int):
            return self._get(
                self.components["x"].iteration_ids[indices], source, ignore_warnings
            )
        elif isinstance(indices, slice):
            return self._get(
                list(self.components["x"].iteration_ids[indices]),
                source,
                ignore_warnings,
            )
        elif isinstance(indices, list) and all(
            isinstance(index, int) for index in indices
        ):
            selected_indices = [
                self.components["x"].iteration_ids[index] for index in indices
            ]
            return self._get(selected_indices, source, ignore_warnings)
        else:
            raise TypeError(
                "indices parameter needs to be provided. It needs to be either an"
                " integer (negative also allowed),"
                " a list of integers, or a slice(int, int) object. \n"
                " Hint: You can also make use of"
                " get_all(),"
                " get_converged(),"
                " or get()."
            )

    def _get(
        self,
        steps: Union[str, int, List[int]] = None,
        source: str = "auto",
        ignore_warnings: bool = False,
    ) -> xarray.DataArray:
        """
        Private Method used to load data for a field. Fields can be loaded for
        one or multiple
        simulation steps. As this method is related to VectorField it will load all
        components of the vector field

        Parameters
        ----------
        steps : Union[str, int, List[int]]
            Which simulation steps to load. Either a single step, a list of steps or
            string "all" to select all available. Providing no value for `steps` works
            with the "scf" calculation mode and will return converged fields.
        source : str
            Target output format for the field. E. g. "z=0", "cube", "vtk", ...
        ignore_warnings : bool
            Choose whether to ignore warnings that to much memory might be used when
            loading.

        Returns
        -------
        xarray.DataArray
            Xarray contains the field, and an additional coordinates "step" for
            selecting the simulation steps and "component" for the components of the
            vectors.

        """
        # Memory check START
        # Prepare list of iterations which shall be loaded
        if isinstance(steps, list):
            load_step = steps
        elif steps == "all":
            load_step = self.components["x"].iteration_ids
        elif isinstance(steps, int):
            load_step = [steps]

        self._check_steps_exist(load_step)

        # Re-use checking of memory from a single field. From VectorField.__init__() we
        # know all components exist in all steps. So we can check memory by trying if
        # loading a ScalarField with all requested iterations would word and multiplying
        # the number of steps by three, as we want to load this many steps for three
        # total components.
        if not ignore_warnings:
            self.components["x"]._check_required_memory(load_step * 3, source)

        dimnames = ["x", "y", "z"]
        compx = self.components["x"]._get(
            steps, source, ignore_warnings=ignore_warnings
        )
        varray = np.empty(
            shape=(len(dimnames), *compx.values.shape), dtype=compx.values.dtype
        )
        varray[0] = compx.values
        varray[1] = (
            self.components["y"]
            ._get(steps, source, ignore_warnings=ignore_warnings)
            .values
        )
        varray[2] = (
            self.components["z"]
            ._get(steps, source, ignore_warnings=ignore_warnings)
            .values
        )

        coords = {"component": dimnames, **compx.coords}
        dims = ("component", *compx.dims)

        return xarray.DataArray(
            varray,
            coords=coords,
            dims=dims,
            name=compx.name[:-2],  # remove the "-x" from name
        )

    def _output_iter_difference(self, x: List[int], y: List[int], z: List[int]) -> str:
        """
        Allows to build a table for the user to check, which simulation steps are
        missing. Setup for `build_dim_string`.

        Parameters
        ----------
        x : List[int]
            All steps found for component x
        y : List[int]
            All steps found for component y
        z : List[int]
            All steps found for component z

        Returns
        -------
        str
            Table style overview of all found steps per component with empty spaces
            when step is missing.
        """
        # get max number of step digits (assume two components are missing (value 0), so
        # we still find the correct value. If two are present, values also is correct
        digits = max(self.components[dim].digits_in_iter_id for dim in "xyz")
        all_iters = list(set(x + y + z))
        components_overview = "".join(
            self._build_dim_string(str_dim, dim, all_iters, digits)
            for (str_dim, dim) in zip("xyz", [x, y, z])
        )
        return components_overview

    def _build_dim_string(
        self, compname: str, dim: List[int], alliters: List[int], digits: int
    ) -> str:
        """
        Build and return a string that contains all the step IDs for a given component.
        If step is missing, whitespaces with the same width are added

        Parameters
        ----------
        compname : str
            Name of the component, used as prefix
        dim : List[int]
            List with all found steps for component `compname`
        alliters : List[int]
            List with all available steps between all components
        digits : int
            Number of characters that can be in the step's number (max)

        Returns
        -------
        str
            Line containing all simulation steps for this component and whitespaces,
            where a step is missing compared to other components.

        """
        # add component name
        res = f"{compname}: "
        for val in alliters:
            if val in dim:
                # add value
                res += f"{val},".rjust(digits + 2)
            else:
                # add whitespace
                res += "".rjust(digits + 2)
        # add newline
        res += "\n"
        return res

    def _check_steps_exist(self, load_steps):
        for step in load_steps:
            # Check if data exists for the requested iteration
            # __init__ guarantees components["x"].iteration_ids
            # == components["y"].iteration_ids == components["z"].iteration_ids
            if step not in self.components["x"].iteration_ids:
                raise ValueError(
                    f"Requested iteration '{step}' does not exist for '{self.ftype}'."
                )


class TDGeneralVectorField(ConvenienceDict):

    __dict_name__ = "dimensions"

    # TODO: CHANGE Doc and test functionality

    def __init__(self, vfield_name: str, fields_in_td_general: List[pathlib.Path]):
        """
         A class for holding the tdgeneral vector fields data.

         It groups all the dimensions of a tdgeneral vector field into one object,
         through a ConvenienceDict.

        Each of the dimensions will hold a pandas DataFrame.

        Parameters
        ----------
        vfield_name: str
         Name of the tdgeneral vector field.
        fields_in_td_general: List[pathlib.Path]
            paths to all the files stored in tdgeneral.
        """
        super().__init__()

        for vfield_comp in fields_in_td_general:
            if vfield_comp.name[:-2] == vfield_name:
                dim = vfield_comp.name[-1]
                _fileobj = openfile(vfield_comp)
                self.dimensions[dim] = _fileobj.values
                # Adding units attribute, NOT a column of the dataframe.
                self.dimensions[dim].attrs = _fileobj.attrs
