import sys

from postopus.octopus_run import Run  # noqa: F401

if sys.version_info >= (3, 8):
    from importlib import metadata
else:
    import importlib_metadata as metadata

__version__ = metadata.version("postopus")
