from pathlib import Path

import xarray

import postopus.files.file


def to_xarray(file_obj: postopus.files.file.File) -> xarray.DataArray:
    """
    Take a file object and return its correspondent xarray.

    Parameters
    ----------
    file_obj : postopus.files.file.File
        File object that to build xarray.DataArray from

    Returns
    -------
    xarray.DataArray
        xarray.DataArray with information from the File

    """
    xarr = xarray.DataArray(file_obj.values, dims=file_obj.dims, coords=file_obj.coords)
    xarr.name = Path(file_obj.filepath).stem
    return xarr
