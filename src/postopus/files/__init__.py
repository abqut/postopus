import pathlib
from typing import Any

from postopus.files.cube import CubeFile
from postopus.files.netcdf import NetCDFFile
from postopus.files.pandas_text import PandasTextFile
from postopus.files.text import TextFile
from postopus.files.vtk import VTKFile
from postopus.files.xcrysden import XCrySDenFile
from postopus.files.xyz import XYZFile

extension_loader_map = {
    ext: cls
    for cls in [
        CubeFile,
        NetCDFFile,
        TextFile,
        VTKFile,
        XYZFile,
        XCrySDenFile,
        PandasTextFile,
    ]
    for ext in cls.EXTENSIONS
}


def openfile(filepath: pathlib.Path) -> Any:
    """
    Will automatically choose the correct class to open a file.

    :param filepath: path to the file that shall be opened
    :return: instance class that was chosen to open the file with postopus file loaders
    """
    try:
        # Get the class to use for loading the file (without '.')
        # [1:] also work with no suffixes (must be a str thing...)
        cls = extension_loader_map[filepath.suffix[1:]]
    except KeyError:
        raise ValueError(
            f"There is no parser for the source '{filepath.suffix[1:]}'! \n"
            f" If you need to read files of this type contact the postopus developer"
            f" team. "
        )
    # instantiate object for file type
    return cls(filepath)
