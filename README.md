# Postopus

Postopus is a project to explore different software designs for postprocessing for Octopus runs.

## Documentation

The documentation of postopus is hosted [here](https://octopus-code.gitlab.io/postopus/index.html)

## Setup
Python & conda versions supported: 3.8, 3.9.

### User Setup
Using a virtual environment is recommended.
To install the project's dependencies, navigate (`cd`) into the project directory and run `pip install .`.

### Developer Setup
This section is for developers wanting to contribute to Postopus.
To setup your development environment, you not only need the to install the dependencies of the project's code itself, but also some modules for testing and keeping the repo clean through pre-commit hooks:

- Installing Postopus with development dependencies in editable mode can be done with `pip install -e .[dev]`
(or `pip install -e '.[dev]'` if you are using mac default's `zsh`) in the project's main directory. If you also want the `docs` requirements you would need to execute `pip install -e .[dev,docs]`

- After installing, you'll need to set up the pre-commit module. Do this with `pre-commit install` in the project's root.

- Tests might require output data from Octopus, for this refer to the section below (Downlaoding example and test data).


## Usage
Example codes in `src/postopus/examples`. The scripts in that folder can be used to easily debug the code.
`example_data_reading.py` shows how to instantiate a `Run` object that allows access to the data from a run with a single System.
`example_achimedeantrial_spiral.py` uses data from a run with multiple systems, providing an example with more complex Octopus output.

To execute these examples, you need to download the test data. (Or compute it, which is harder.)

## Downloading example and test data

A prepared set of output files can be downloaded by running ``cd tests/data && python download_testdata.py``.

### Jupyter Notebook
Check out the Jupyter Notebooks in `docs/notebooks`. They might be more understandable and provide some inline documentation over the scripts in `src/postopus/examples`.

-------------------

### Advanced: Build Example Data for Testing

Most users and developers can ignore this section.

Once you have downloaded the test data (and thus the `inp` files of the example data sets, you can in principle run octopus to compute the output data. We plan to use this later for more complete continuous integration. Here are the instructions for that procedure:

Several input files for smaller Octopus runs can be found in `tests/data`.
The script `tests/data/build_testdata.py` helps to run these examples and generates outputs.
An Octopus binary is required for execution. You can either provide one yourself by
providing the path to a binary with `-o` or you let the script build a Docker image and
run Octopus from inside a container with `-d`. The latter option might require root on
Linux, because Docker - if you need to use `sudo`, make sure to use
`/path/to/virtualenv/bin/python` as the Python Interpreter if you use Virtual
Environments or similar.
Parameters for `build_testdata.py`:
- `--octopus /path/to/octopus/binary`, `--o /path/to/octopus/binary` use a custom Octopus binary
- `--docker`, `-d` build an image with Docker and use it to execute Octopus
- `--parallel mpi_tasks`, `-p mpi_tasks` run Octopus with `#processes` MPI processes
- `--verbose`, `-v` show output of Octopus on your STDOUT
- `--force`, `-f` overwrite existing results
- `--help`, `-h` show help on usage

Testdata can also be build with tox: `tox -e testdata -- [parameters for build_testdata.py]`.
Note that this is problematic with Docker due to the required permissions messing with
permissions in the `.tox` directory.
