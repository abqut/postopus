import re

import postopus


def test_version_string():
    """setuptools_scm has a convenient function to provide a version string which
    either takes the release tag or a combination of that and a git hash if the
    current version if from a git repository.

    Here is an example:

      0.1.dev163+g92f9d0f.d20211111

    I think this reads as:
    - based on 0.1
    - development that is 163 aheads of 0.1
    - what follows the "+g" is a git hash of the current git HEAD, i.e. 92f9d0f
    - what follows the ".d" is today's date (or the date of the commit?)

    The version string is roughly what git would report with "git describe
    --dirty --tags --long". For details, please consult
    https://github.com/pypa/setuptools_scm/

    """

    ver = postopus.__version__

    # Very coarse tests
    assert isinstance(ver, str)
    assert len(ver) < 40
    assert len(ver) > 1
    assert "." in ver

    # An attempt to be a bit stricter:
    #
    #   The regular expression below is for simplicity not very strict:
    #   we allow arbitrary strings in between the parts we know
    #   must or can exist (we only test for a subset of available options)
    pattern = re.compile(
        r"""
        .?
        [\d.]+        # `major.minor[.patch]`
        .?
        (g[0-9a-f]+)?  # optional git commit hash
        .?
        (d\d+)?     # optional timestamp
        .?
        """,
        re.VERBOSE,
    )
    assert pattern.match(postopus.__version__)
